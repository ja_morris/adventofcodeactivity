/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day4;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author James
 */
public class Day4Part1 {
    private static final String input = "ckczppom";
    
    public static void main(String[] args){
        byte[] finalData;
        int i = 0;
        boolean run = true;
        boolean found;
        try {
            do{
                String temp = convertByteArrayToHexString(finalData = parseData(input + i++));
                found = true;
                for(int j = 0; j < 5; j++){
                    if(temp.charAt(j) != '0')
                        found = false;
                }
                if(found)
                    run = false;
            }while(run);
            System.out.println(i-1 + "");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(Day4Part1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static byte[] parseData(String parseData) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        byte[] bytesOfMessage = parseData.getBytes("UTF-8");

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(bytesOfMessage);
        return thedigest;
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }
}
