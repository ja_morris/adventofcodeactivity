package Day5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
        --- Day 5: Doesn't He Have Intern-Elves For This? ---

        Santa needs help figuring out which strings in his text file are naughty or nice.

        A nice string is one with all of the following properties:

        It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
        It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
        It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
        For example:

        ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
        aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
        jchzalrnumimnmhp is naughty because it has no double letter.
        haegwjzuvuyypxyu is naughty because it contains the string xy.
        dvszwmarrgswjxmb is naughty because it contains only one vowel.
        How many strings are nice?
*/


/**
 *
 * @author James
 */
public class Day5Part1 {
    private static final String FILE_LOCATION = "inputs/inputDay5.txt"; //LOCATION OF THE FILE
    private static final String[] poorInput = {"ab", "cd", "pq", "xy"}; //LIST OF SUBSTRINGS THAT WILL REJECT
    private static final char[] goodInput = {'a', 'e', 'i', 'o', 'u'}; //LIST OF VOWELS OF WHICH TO CHECK
    
    /**
     * This will check whether or not the parameter String has a bad input when checked against
     * the 'poorInput' value set.
     * @param input A string which denotes the current input.
     * @return True if the input string contains a substring
     * defined in 'poorInput' the check and false if it does not.
     */
    private static boolean hasPoorInput(String input) {
        for(int i = 0; i < poorInput.length; i++)
            if(input.contains(poorInput[i])) 
                return true;
        return false;
    }
    
    /**
     * Will check the input String for its having more than 3 of any combination
     * of values from the 'goodInput' character set. Moreover, this function will
     * also determine whether or not the function has at least 1 repeated character
     * which appears twice in a row.
     * @param input A string which denotes the current input.
     * @return True if and only if both input has at least three instances of having
     * vowels and if there is a character that is repeated twice in a row.
     */
    private static boolean hasGoodInput(String input) {
        int count = 0;
        for(int i = 0; i < input.length(); i++)
            for(int j = 0; j < goodInput.length; j++)
                if(input.charAt(i) == goodInput[j])
                    count++;
        char temp = input.charAt(0);
        boolean hasFound = false;
        for(int i = 1; i < input.length(); i++){
            if(temp == input.charAt(i)){
                hasFound = true;
                break;
            }
            temp = input.charAt(i);
        }
        return count>=3 && hasFound;
    }
    
    public static void main(String[] arg0){
       File file = new File(FILE_LOCATION);
        Scanner scan;
        String parse;
        if(!file.exists())System.out.println("Error in Reading File.");
        
        int elementCount = 0;
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                parse = scan.nextLine();
                if(hasGoodInput(parse) && !hasPoorInput(parse))
                    elementCount++;
            }
            scan.close();
            System.out.println(elementCount);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }   
    }
}
