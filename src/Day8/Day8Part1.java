/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author James
 */
public class Day8Part1 {
    private static final String FILE_LOCATION = "inputs/inputDay8.txt";
    
    public static void main(String[] arg0){
        int totalSize = 0;
        File file = new File(FILE_LOCATION);
        try{
            Scanner scan = new Scanner(file);
            while(scan.hasNextLine()){
                totalSize += parseData(scan.nextLine());
            }
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        System.out.println(String.valueOf(totalSize));
    }
    
    private static int parseData(String temp){
        int size = 0;
        for(int i = 1; i < temp.length(); i++){
            if(i == temp.length()-1)
                continue;
            if(temp.charAt(i) == '\\'){
                if(temp.charAt(i+1) == 'x'){
                    size++;
                    i+=3;
                }
                else if(temp.charAt(i+1) == '\\'){
                    size++;
                    i++;
                }
                else if(temp.charAt(i+1) == '"'){
                    size++;
                    i++;
                }
            }
            else if(temp.charAt(i) != '"')
                size++;
        }
        return temp.length() - size;
    }
}
