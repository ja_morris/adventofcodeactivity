/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author James
 */
public class Day8Part2 {
    private static final String FILE_LOCATION = "inputs/inputDay8.txt";
    
    public static void main(String[] arg0){
        int totalSize = 0;
        File file = new File(FILE_LOCATION);
        try{
            Scanner scan = new Scanner(file);
            while(scan.hasNextLine()){
                totalSize += parseData(scan.nextLine());
            }
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        System.out.println(String.valueOf(totalSize));
    }

    private static int parseData(String dataToParse){
        int size = 0;
        char[] newInput = dataToParse.toCharArray();
        for(int i = 0; i < newInput.length; i++){
            if(newInput[i] == '"')
                size+=2;
            else if(newInput[i] == '\\')
                size+=2;
            else
                size++;
        }
        size += 2;
        return size - dataToParse.length();
    }
}

