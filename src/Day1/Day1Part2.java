/*
    --- Part Two ---

    Now, given the same instructions, find the position of the first character that causes him to enter the basement (floor -1). The first character in the instructions has position 1, the second character has position 2, and so on.

    For example:

    ) causes him to enter the basement at character position 1.
    ()()) causes him to enter the basement at character position 5.
    What is the position of the character that causes Santa to first enter the basement?
*/

package Day1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author James
 */
public class Day1Part2 {
    private static final String FILE_LOCATION = "inputs/inputDay1.txt"; //LOCATION OF THE FILE LOCATION
    public static void main(String[] args){
        try{
            int keepTrack = 0;
            File file = new File(FILE_LOCATION);
            Scanner scan = new Scanner(file);
            while(scan.hasNextLine()){
                String parseString = scan.nextLine();
                for (int i = 0; i < parseString.length(); i++){
                    if(parseString.charAt(i) == ' ') continue;
                    else if(parseString.charAt(i) == '(') keepTrack++;
                    else if(parseString.charAt(i) == ')') keepTrack--;

                    if(keepTrack < 0){
                        keepTrack = i+1;
                        i = parseString.length();
                        continue;
                    }
                }
            }
            System.out.println(String.valueOf(keepTrack));
        }catch(FileNotFoundException ex){ex.printStackTrace();}
    }
}
