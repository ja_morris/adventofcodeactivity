    package Day6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
    --- Part Two ---

    You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

    The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

    The phrase turn on actually means that you should increase the brightness of those lights by 1.

    The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

    The phrase toggle actually means that you should increase the brightness of those lights by 2.

    What is the total brightness of all lights combined after following Santa's instructions?

    For example:

    turn on 0,0 through 0,0 would increase the total brightness by 1.
    toggle 0,0 through 999,999 would increase the total brightness by 2000000.
*/


/**
 *
 * @author James
 */
public class Day6Part2 {
    private static final String FILE_LOCATION = "inputs/inputDay6.txt"; //LOCATION OF THE FILE
    private static final String STRING_DELIMITER = " ";
    private static final String TOGGLE = "toggle";
    private static final String TURN = "turn";
    private static final String TURN_ON = "on";
    private static final String TURN_OFF = "off";
    
    private static final int DIM = 1001;
    private static final int ARR[][] = new int[DIM][DIM];
    
    public static void main(String[] arg0){
       File file = new File(FILE_LOCATION);
        Scanner scan;
        String parse;
        if(!file.exists())System.out.println("Error in Reading File.");
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                parse = scan.nextLine();
                String[] tempLine = parse.split(STRING_DELIMITER);
                int[] dimensions = new int[4];
                int intent;
                
                if(tempLine[0].equals(TURN)){
                    if(tempLine[1].equals(TURN_ON)){
                        intent = 1;
                    }
                    else{
                        intent = 0;
                    }
                    String[] temp1;
                    temp1 = tempLine[2].split(",");
                    dimensions[0] = Integer.parseInt(temp1[0]);
                    dimensions[1] = Integer.parseInt(temp1[1]);
                    temp1 = tempLine[4].split(",");
                    dimensions[2] = Integer.parseInt(temp1[0]);
                    dimensions[3] = Integer.parseInt(temp1[1]);
                    
                }
                else{
                    intent = 2;
                    String[] temp1;
                    temp1 = tempLine[1].split(",");
                    dimensions[0] = Integer.parseInt(temp1[0]);
                    dimensions[1] = Integer.parseInt(temp1[1]);
                    temp1 = tempLine[3].split(",");
                    dimensions[2] = Integer.parseInt(temp1[0]);
                    dimensions[3] = Integer.parseInt(temp1[1]);
                }
                augmentLights(intent, dimensions);
            }
            scan.close();
                System.out.println(numLights());

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * This method applies changes to the array grid.
     * @param intent 0 represents the turning off of lights, 1 represents the turning on of lights, and 2 represents the swapping of 0 and 1.
     * @param dimensions A Cartesian coordinate pair for the beginning and ending values.
     */
    private static void augmentLights(int intent, int[] dimensions){
        for(int i = dimensions[1]; i <= dimensions[3]; i++){
            for(int j = dimensions[0]; j <= dimensions[2]; j++){
                if(intent == 0){
                    if(--ARR[i][j] < 0)
                        ARR[i][j] = 0;
                }
                else if(intent == 1)
                    ARR[i][j]++;
                else if( intent == 2)
                    ARR[i][j]+=2;
            }
        }
    }
    
    /**
     * Counts the number of lights that have been activated.
     * @return The number of activated lights.
     */
    private static int numLights(){
        int count = 0;
        for(int i = 0; i < DIM; i++){
            for(int j = 0; j < DIM; j++)
                count += ARR[i][j];
        }
        return count;
    }
    
    /**
     * Prints the graphical representation of the status of the array.
     */
    private static void print () {
        System.out.println("\n");
        for(int i = 0; i < DIM; i++){
            for(int j = 0; j < DIM; j++)
                System.out.print(ARR[i][j] + " ");
            System.out.println();
        }
    }
}

