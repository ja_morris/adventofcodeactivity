    package Day6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
    --- Day 6: Probably a Fire Hazard ---

    Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

    Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

    Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

    To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

    For example:

    turn on 0,0 through 999,999 would turn on (or leave on) every light.
    toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
    turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
    After following the instructions, how many lights are lit?
*/


/**
 *
 * @author James
 */
public class Day6Part1 {
    private static final String FILE_LOCATION = "inputs/inputDay6.txt"; //LOCATION OF THE FILE
    private static final String STRING_DELIMITER = " ";
    private static final String TOGGLE = "toggle";
    private static final String TURN = "turn";
    private static final String TURN_ON = "on";
    private static final String TURN_OFF = "off";
    
    private static final int DIM = 1001;
    private static final int ARR[][] = new int[DIM][DIM];
    
    public static void main(String[] arg0){
       File file = new File(FILE_LOCATION);
        Scanner scan;
        String parse;
        if(!file.exists())System.out.println("Error in Reading File.");
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                parse = scan.nextLine();
                String[] tempLine = parse.split(STRING_DELIMITER);
                int[] dimensions = new int[4];
                int intent;
                
                if(tempLine[0].equals(TURN)){
                    if(tempLine[1].equals(TURN_ON)){
                        intent = 1;
                    }
                    else{
                        intent = 0;
                    }
                    String[] temp1;
                    temp1 = tempLine[2].split(",");
                    dimensions[0] = Integer.parseInt(temp1[0]);
                    dimensions[1] = Integer.parseInt(temp1[1]);
                    temp1 = tempLine[4].split(",");
                    dimensions[2] = Integer.parseInt(temp1[0]);
                    dimensions[3] = Integer.parseInt(temp1[1]);
                    
                }
                else{
                    intent = 2;
                    String[] temp1;
                    temp1 = tempLine[1].split(",");
                    dimensions[0] = Integer.parseInt(temp1[0]);
                    dimensions[1] = Integer.parseInt(temp1[1]);
                    temp1 = tempLine[3].split(",");
                    dimensions[2] = Integer.parseInt(temp1[0]);
                    dimensions[3] = Integer.parseInt(temp1[1]);
                }
                augmentLights(intent, dimensions);
            }
            scan.close();
                System.out.println(numLights());

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * This method applies changes to the array grid.
     * @param intent 0 represents the turning off of lights, 1 represents the turning on of lights, and 2 represents the swapping of 0 and 1.
     * @param dimensions A Cartesian coordinate pair for the beginning and ending values.
     */
    private static void augmentLights(int intent, int[] dimensions){
        for(int i = dimensions[1]; i <= dimensions[3]; i++){
            for(int j = dimensions[0]; j <= dimensions[2]; j++){
                if(intent == 0)
                    ARR[i][j] = 0;
                else if(intent == 1)
                    ARR[i][j] = 1;
                else if( intent == 2)
                    ARR[i][j] = (ARR[i][j]==0 ? 1 : 0);
            }
        }
    }
    
    /**
     * Counts the number of lights that have been activated.
     * @return The number of activated lights.
     */
    private static int numLights(){
        int count = 0;
        for(int i = 0; i < DIM; i++){
            for(int j = 0; j < DIM; j++)
                count += ARR[i][j];
        }
        return count;
    }
    
    /**
     * Prints the graphical representation of the status of the array.
     */
    private static void print () {
        System.out.println("\n");
        for(int i = 0; i < DIM; i++){
            for(int j = 0; j < DIM; j++)
                System.out.print(ARR[i][j] + " ");
            System.out.println();
        }
    }
}
