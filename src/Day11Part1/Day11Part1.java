package Day11Part1;
/*
    --- Day 11: Corporate Policy ---

    Santa's previous password expired, and he needs help choosing a new one.

    To help him remember his new password after the old one expires, Santa has devised a method of coming up with a password based on the previous one. Corporate policy dictates that passwords must be exactly eight lowercase letters (for security reasons), so he finds his new password by incrementing his old password string repeatedly until it is valid.

    Incrementing is just like counting with numbers: xx, xy, xz, ya, yb, and so on. Increase the rightmost letter one step; if it was z, it wraps around to a, and repeat with the next letter to the left until one doesn't wrap around.

    Unfortunately for Santa, a new Security-Elf recently started, and he has imposed some additional password requirements:

    Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
    Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
    Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.
    For example:

    hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
    abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
    abbcegjk fails the third requirement, because it only has one double letter (bb).
    The next password after abcdefgh is abcdffaa.
    The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.
    Given Santa's current password (your puzzle input), what should his next password be?
*/

/**
 *
 * @author James
 */
public class Day11Part1 {
    private static final String INPUT = "hxbxwxba";
    private static final char[] BAD_INPUT = {'i', 'o', 'l'};
    private static String updatedString = INPUT;
    private static int count;
    
    public static void main(String[] args){
        boolean run = true;

        do{
            incrementString();
            if(!containsPoorInput() && containsARow() && containsDoubleInput())
                run = false;
        }while(run);
        System.out.println(updatedString);
        System.out.println(count);
    }
    
    /**
     * Converts the current String into a char array.
     * @return A usable array of characters.
     */
    private static char[] convertInput(){
        return updatedString.toCharArray();
    }
    
    /**
     * increments the String in  the fashion listed in the directions.
     */
    private static void incrementString(){
        char[] temp = convertInput();
        boolean isRunning = true;
        int currentIndex = temp.length-1;
        while(isRunning){
            if(++temp[currentIndex]>'z')
                temp[currentIndex--] = 'a';
            else
                isRunning = false;
        }
        updatedString = new String(temp);
        count++;
    }
    
    /**
     * Checks to see if the input has invalid characters.
     * @return True if contains poor input and false if it contains correct input.
     */
    private static boolean containsPoorInput( ){
        char[] temp = convertInput();
        for(int i = 0; i < temp.length; i++)
            for(int j = 0; j < BAD_INPUT.length; j++)
                if(temp[i] == BAD_INPUT[j])
                    return true;
        return false;
    }
    
    /**
     * Checks to see if the input has 3 individual characters in a row.
     * @return True if contains 3 characters incrementally and false if it does not.
     */
    private static  boolean containsARow( ){
        char[] temp = convertInput();
        for(int i = 0; i < temp.length-2; i++)
            if(temp[i]+1 == temp[i+1] && temp[i]+2 == temp[i+2])
                return true;
        return false;
    }
    
    /**
     * Checks to see if the input has at least two character in adjacent to one another.
     * @return True if contains a doubling and false if it does not contain a doubling.
     */
    private static boolean containsDoubleInput (){
        char[] temp = convertInput();
        char previousChar = temp[0];
        int firstPosition = -1;
        for(int i = 1; i < temp.length; i++){
            if(temp[i] == previousChar){
                firstPosition = i;
                break;
            }
            previousChar = temp[i];
        }  
        if(firstPosition < 0)
            return false;
        
        previousChar = temp[0];
        int secondPosition = firstPosition - 1;
        for(int i = 1; i < temp.length; i++){
            if(i == firstPosition || i == secondPosition){
                if(firstPosition+2>=temp.length)
                    break;
                previousChar = temp[firstPosition+1];
                i = firstPosition+1;
                continue;
            }
            if(temp[i] == previousChar)
                return true;
            previousChar = temp[i];
        }
        return false;
    }
}
