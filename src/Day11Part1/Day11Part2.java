package Day11Part1;
/*
    Your puzzle answer was hxbxxyzz.

    The first half of this puzzle is complete! It provides one gold star: *

    --- Part Two ---

    Santa's password expired again. What's the next one?

    Your puzzle input is still hxbxwxba.
*/

/**
 *
 * @author James
 */
public class Day11Part2 {
    private static final String INPUT = "hxbxxyzz";
    private static final char[] BAD_INPUT = {'i', 'o', 'l'};
    private static String updatedString = INPUT;
    private static int count;
    
    public static void main(String[] args){
        boolean run = true;

        do{
            incrementString();
            if(!containsPoorInput() && containsARow() && containsDoubleInput())
                run = false;
        }while(run);
        System.out.println(updatedString);
        System.out.println(count);
    }
    
    /**
     * Converts the current String into a char array.
     * @return A usable array of characters.
     */
    private static char[] convertInput(){
        return updatedString.toCharArray();
    }
    
    /**
     * increments the String in  the fashion listed in the directions.
     */
    private static void incrementString(){
        char[] temp = convertInput();
        boolean isRunning = true;
        int currentIndex = temp.length-1;
        while(isRunning){
            if(++temp[currentIndex]>'z')
                temp[currentIndex--] = 'a';
            else
                isRunning = false;
        }
        updatedString = new String(temp);
        count++;
    }
    
    /**
     * Checks to see if the input has invalid characters.
     * @return True if contains poor input and false if it contains correct input.
     */
    private static boolean containsPoorInput( ){
        char[] temp = convertInput();
        for(int i = 0; i < temp.length; i++)
            for(int j = 0; j < BAD_INPUT.length; j++)
                if(temp[i] == BAD_INPUT[j])
                    return true;
        return false;
    }
    
    /**
     * Checks to see if the input has 3 individual characters in a row.
     * @return True if contains 3 characters incrementally and false if it does not.
     */
    private static  boolean containsARow( ){
        char[] temp = convertInput();
        for(int i = 0; i < temp.length-2; i++)
            if(temp[i]+1 == temp[i+1] && temp[i]+2 == temp[i+2])
                return true;
        return false;
    }
    
    /**
     * Checks to see if the input has at least two character in adjacent to one another.
     * @return True if contains a doubling and false if it does not contain a doubling.
     */
    private static boolean containsDoubleInput (){
        char[] temp = convertInput();
        char previousChar = temp[0];
        int firstPosition = -1;
        for(int i = 1; i < temp.length; i++){
            if(temp[i] == previousChar){
                firstPosition = i;
                break;
            }
            previousChar = temp[i];
        }  
        if(firstPosition < 0)
            return false;
        
        previousChar = temp[0];
        int secondPosition = firstPosition - 1;
        for(int i = 1; i < temp.length; i++){
            if(i == firstPosition || i == secondPosition){
                if(firstPosition+2>=temp.length)
                    break;
                previousChar = temp[firstPosition+1];
                i = firstPosition+1;
                continue;
            }
            if(temp[i] == previousChar)
                return true;
            previousChar = temp[i];
        }
        return false;
    }
}
