
package Day10Part1;

import java.util.ArrayList;

/*
    --- Day 10: Elves Look, Elves Say ---

    Today, the Elves are playing a game called look-and-say. They take turns making sequences by reading aloud the previous sequence and using that reading as the next sequence. For example, 211 is read as "one two, two ones", which becomes 1221 (1 2, 2 1s).

    Look-and-say sequences are generated iteratively, using the previous value as input for the next step. For each step, take the previous value, and replace each run of digits (like 111) with the number of digits (3) followed by the digit itself (1).

    For example:

    1 becomes 11 (1 copy of digit 1).
    11 becomes 21 (2 copies of digit 1).
    21 becomes 1211 (one 2 followed by one 1).
    1211 becomes 111221 (one 1, one 2, and two 1s).
    111221 becomes 312211 (three 1s, two 2s, and one 1).
    Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?

*/


/**
 *
 * @author James
 */
public class Day10Part1 {
    private static String INPUT = "3113322113";
    private static ArrayList<Integer> list;
    private static final int NUM_ITERATIONS = 40;
    
    public static void main(String[] arg0){
        list = convertInput();
        for(int i = 0; i < NUM_ITERATIONS; i++){
            performIteration();
        }
        print();
    }
    
    /**
     * Calculates and prints the size of the sequence to the console.
     */
    private static void print(){
        System.out.println(list.size());
        for(int i = 0; i < list.size(); i++)
            System.out.print(list.get(i));
        System.out.println("");
    }
    
    /**
     * Converts the initial input into arrayList form.
     * @return An arraylist with each index as a single character.
     */
    private static ArrayList<Integer> convertInput (){
        ArrayList<Integer> temp = new ArrayList();
        for(int i = 0; i < INPUT.length(); i++)
            temp.add(INPUT.charAt(i) - '0');
        return temp;
    }

    /**
     * computes the next iteration of the process.
     */
    private static void performIteration(){
        ArrayList<Integer> container = new ArrayList();     
        int currentItem = 0;
        int currentHeld = 0;
        for(int i = 0; i < list.size(); i++){
            if(currentItem == 0 || list.get(i) != currentItem){
                if(i != 0) currentHeld++;
                currentItem = list.get(i);
                for(int j = 0; j < 2; j++)
                    container.add(0);
                container.set((currentHeld*2)+1, currentItem);
                container.set(currentHeld*2, container.get(currentHeld*2) +1);
            }
            else{
                container.set(currentHeld*2, container.get(currentHeld*2) +1);
            } 
        }
        list = container;
        System.out.println("");
    }

}
