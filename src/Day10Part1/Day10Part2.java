package Day10Part1;

import java.util.ArrayList;



/*
    --- Part Two ---

    Neat, right? You might also enjoy hearing John Conway talking about this sequence (that's Conway of Conway's Game of Life fame).

    Now, starting again with the digits in your puzzle input, apply this process 50 times. What is the length of the new result?

    Your puzzle input is still 3113322113.
*/
/**
 *
 * @author James
 */
public class Day10Part2 {
    private static String INPUT = "3113322113";
    private static ArrayList<Integer> list;
    private static final int NUM_ITERATIONS = 50;
    
    public static void main(String[] arg0){
        list = convertInput();
        for(int i = 0; i < NUM_ITERATIONS; i++){
            performIteration();
        }
        print();
    }
    
    /**
     * Calculates and prints the size of the sequence to the console.
     */
    private static void print(){
        System.out.println(list.size());
        for(int i = 0; i < list.size(); i++)
            System.out.print(list.get(i));
        System.out.println("");
    }
    
    /**
     * Converts the initial input into arrayList form.
     * @return An arraylist with each index as a single character.
     */
    private static ArrayList<Integer> convertInput (){
        ArrayList<Integer> temp = new ArrayList();
        for(int i = 0; i < INPUT.length(); i++)
            temp.add(INPUT.charAt(i) - '0');
        return temp;
    }

     /**
     * computes the next iteration of the process.
     */
    private static void performIteration(){
        ArrayList<Integer> container = new ArrayList();     
        int currentItem = 0;
        int currentHeld = 0;
        for(int i = 0; i < list.size(); i++){
            if(currentItem == 0 || list.get(i) != currentItem){
                if(i != 0) currentHeld++;
                currentItem = list.get(i);
                for(int j = 0; j < 2; j++)
                    container.add(0);
                container.set((currentHeld*2)+1, currentItem);
                container.set(currentHeld*2, container.get(currentHeld*2) +1);
            }
            else{
                container.set(currentHeld*2, container.get(currentHeld*2) +1);
            } 
        }
        list = container;
        System.out.println("");
    }

}