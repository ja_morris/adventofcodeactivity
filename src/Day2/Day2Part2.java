/*
    --- Part Two ---

    The elves are also running low on ribbon. Ribbon is all the same width, so they only have to worry about the length they need to order, which they would again like to be exact.

    The ribbon required to wrap a present is the shortest distance around its sides, or the smallest perimeter of any one face. Each present also requires a bow made out of ribbon as well; the feet of ribbon required for the perfect bow is equal to the cubic feet of volume of the present. Don't ask how they tie the bow, though; they'll never tell.

    For example:

    A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
    A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.
    How many total feet of ribbon should they order?
*/

package Day2;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author James
 */
public class Day2Part2 {
    private static final String FILE_LOCATION = "inputs/inputDay2.txt";
    private static final String SPLIT_DELIMITER = "x";
    public static void main(String[] arg0){
        File file = new File(FILE_LOCATION);
        Scanner scan;
        int dimX, dimY, dimZ;
        int totalArea = 0;
        String[] token;
        
        if(!file.exists())System.out.println("Error in Reading File.");
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                token = scan.nextLine().split(SPLIT_DELIMITER);
                dimX = Integer.parseInt(token[0]);
                dimY = Integer.parseInt(token[1]);
                dimZ = Integer.parseInt(token[2]);
                
                totalArea += (generateVolume(dimX, dimY, dimZ) 
                             + generateShortestLocation(dimX, dimY, dimZ));
            }
            scan.close();
            System.out.println(String.valueOf(totalArea));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } 
    }
    /**
     * Generates the volume of the box.
     * @param x An int which represents the length.
     * @param y An int which represents the width.
     * @param z An int which represents the depth.
     * @return The volume.
     */
    private static int generateVolume(int x, int y, int z){
        return x * y * z;
    }
    
    /**
     * Generates the Shortest location of all the sides of the box.
     * @param x An int which represents the length.
     * @param y An int which represents the width.
     * @param z An int which represents the depth.
     * @return The shortest location across the box.
     */
    private static int generateShortestLocation(int x, int y, int z){
        int smallestPerimeter = Math.min(Math.min(2*x+2*y, 2*y+2*z), 2*x+2*z);
        int smallestLocation = Math.min(2*y + 2*z, 2*y + 2*x);
        return Math.min(smallestPerimeter, smallestLocation);
    }
}
