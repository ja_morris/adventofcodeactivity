/*
    --- Day 2: I Was Told There Would Be No Math ---

    The elves are running low on wrapping paper, and so they need to submit an order for more. They have a list of the dimensions (length l, width w, and height h) of each present, and only want to order exactly as much as they need.

    Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating the required wrapping paper for each gift a little easier: find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.

    For example:

    A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
    A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.
    All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?
*/

package Day2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author James
 */
public class Day2Part1 {
    private static final String FILE_LOCATION = "inputs/inputDay2.txt"; //ADDRESS OF THE FILE LOCATION
    private static final String SPLIT_DELIMITER = "x"; //THE DELIMITER TO SEPARATE EACH CHARACTER
    public static void main(String[] arg0){
        
        File file = new File(FILE_LOCATION);
        Scanner scan;
        int dimX, dimY, dimZ;
        int totalArea = 0;
        String[] token;
        
        if(!file.exists())System.out.println("Error in Reading File.");
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                token = scan.nextLine().split(SPLIT_DELIMITER);
                dimX = Integer.parseInt(token[0]);
                dimY = Integer.parseInt(token[1]);
                dimZ = Integer.parseInt(token[2]);
                
                totalArea += (generateSquareFootage(dimX, dimY, dimZ) 
                             + generateShortestArea(dimX, dimY, dimZ));
            }
            scan.close();
            System.out.println(String.valueOf(totalArea));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * Generates the square footage.
     * @param x An int which represents the length.
     * @param y An int which represents the width.
     * @param z An int which represents the depth.
     * @return The square footage in integer form.
     */
    private static int generateSquareFootage(int x, int y, int z){
        return 2*x*y + 2*y*z + 2*z*x;
    }
    /**
     * Generates the Shortest area of all the sides of the box.
     * @param x An int which represents the length.
     * @param y An int which represents the width.
     * @param z An int which represents the depth.
     * @return The shortest area.
     */
    private static int generateShortestArea(int x, int y, int z){
        return Math.min(Math.min(x*y, y*z), x*z);
    }
}
