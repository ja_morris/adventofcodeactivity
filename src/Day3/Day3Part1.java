/*
    --- Day 3: Perfectly Spherical Houses in a Vacuum ---

    Santa is delivering presents to an infinite two-dimensional grid of houses.

    He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west (<). After each move, he delivers another present to the house at his new location.

    However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and Santa ends up visiting some houses more than once. How many houses receive at least one present?

    For example:

    > delivers presents to 2 houses: one at the starting location, and one to the east.
    ^>v< delivers presents to 4 houses in a square, including twice to the house at his starting/ending location.
    ^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.
*/

package Day3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author James
 */
public class Day3Part1 {
    private static final String FILE_LOCATION = "inputs/inputDay3.txt";
    private static final int START_SIZE = 3;
    private static int currentSize = START_SIZE;
    
    private static ArrayList<ArrayList> primaryVector;
    private static int coordX = (START_SIZE/2), coordY = (START_SIZE/2);
    
    public static void main(String[] arg0){
        primaryVector = initializeList();
        givePresent();
        
        File file = new File(FILE_LOCATION);
        Scanner scan;
        String parse;
        if(!file.exists())System.out.println("Error in Reading File.");
        int elementCont = 0;
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                parse = scan.nextLine();
                for(int i = 0; i < parse.length(); i++){
                    printData();
                    elementCont++;
                    if(parse.charAt(i) == '>'){
                        moveEast();
                    }
                    else if(parse.charAt(i) == '<'){
                        moveWest();
                    }
                    else if(parse.charAt(i) == '^'){
                        moveNorth();
                    }
                    else if(parse.charAt(i) == 'v'){
                       moveSouth();
                    }
                }
            }
            scan.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } 
        

        printData();
        System.out.println(countHomes()+ "");
    }
    
    /**
     * Moves Santa one unit north, allowing him to visit the house.
     */
    private static void moveNorth(){
        if(--coordY < 0)
            decreaseArrayListSizes(false);
        givePresent();
    }
    
    /**
     * Moves Santa one unit South, allowing him to visit the house.
     */
    private static void moveSouth(){
        if(++coordY >= currentSize)
            increaseArrayListSizes();
        givePresent();
    }
    
    /**
     * Moves Santa one unit East, allowing him to visit the house.
     */
    private static void moveEast(){
        if(++coordX >= currentSize)
            increaseArrayListSizes();
        givePresent();
    }
    
    /**
     * Moves Santa one unit West allowing him to visit the house.
     */
    private static void moveWest(){
        if(--coordX < 0)
            decreaseArrayListSizes(true);
        givePresent();
    }
    
    /**
     * Places a present at the house at which Santa is visiting.
     */
    private static void givePresent(){
        Integer temp = (Integer) primaryVector.get(coordY).get(coordX);
        primaryVector.get(coordY).set(coordX, ++temp);
    }
    
    /**
     * If Santa moves into an area where the index is less than 0, this method
     * will correct that and place him in a modified array where he is at some
     * index 0.
     * @param isHorizontal determines whether he is in a position where the index
     * where he is at is on a horizontal plane.
     */
    private static void decreaseArrayListSizes(boolean isHorizontal){
        currentSize++;
        for(int i = 0; i < primaryVector.size(); i++)
            primaryVector.get(i).add(0, 0);
        ArrayList<Integer> temp = new ArrayList(currentSize);
        primaryVector.add(0, temp);
        while(temp.size() < currentSize)
            temp.add(0);
        if(isHorizontal){
            coordX = 0;
            coordY++;
        }
        else{
            coordY = 0;
            coordX++;
        }
    }
    
    /**
     * This occurs when Santa reaches a house that is indexed by an integer greater
     * than the number in currentSize. This will increase the size of both the Array
     * that houses the vertical and the array that houses the horizontal values.
     * Maintains the n X n characteristics of the value.
     */
    private static void increaseArrayListSizes(){
        ArrayList<Integer> temp;
        currentSize++;
        while(primaryVector.size() < currentSize){
            temp = new ArrayList(currentSize);
            primaryVector.add(temp);
            while(temp.size() < currentSize)
                temp.add(0);
        }
        for(int i = 0; i < currentSize; i++){
            while(primaryVector.get(i).size() < currentSize)
                primaryVector.get(i).add(0);
        }
    }
    /**
     * Creates the array.
     * @return an arraylist that contains the initialized settings for the list.
     */
    private static ArrayList<ArrayList> initializeList(){
        ArrayList<ArrayList> coordVector = new ArrayList(START_SIZE);
        ArrayList<Integer> temp;
        while(coordVector.size() < START_SIZE){
            temp = new ArrayList(START_SIZE);
            coordVector.add(temp);
            while(temp.size() < START_SIZE )
                temp.add(0);
        }
        return coordVector;
    }
    
    /**
     * A method which prints the graphical representation of each step. DO NOT
     * USE if attempting to solve this puzzle for long input strings (such as the one in the input)
     * because it will largely slow the process.
     */
    private static void printData(){
        System.out.println("\n\n\n\n\n");
        for(int i = 0; i < currentSize; i++){
            for(int j = 0; j < currentSize; j++)
                System.out.print(primaryVector.get(i).get(j) + " ");
            System.out.print("\n");
        }
        System.out.println("\n\n\n\n\n");
    }
    
    /**
     * Counts the number of houses Santa has visited at least once.
     * @return The number of houses on which Santa has visited.
     */
    private static int countHomes(){
       int count = 0;
       for(int i = 0; i < currentSize; i++){
            for(int j = 0; j < currentSize; j++)
                if(((Integer)primaryVector.get(i).get(j))>0)
                    count++;
        }
       return count;
    }
    
}
