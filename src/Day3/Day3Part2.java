/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author James
 */
public class Day3Part2 {
    private static final String FILE_LOCATION = "inputs/inputDay3.txt";
    private static final int START_SIZE = 3;
    private static int currentSize = START_SIZE;
    
    private static ArrayList<ArrayList> primaryVector;
    private static int coordXA = (START_SIZE/2), coordYA = (START_SIZE/2);
    private static int coordXB = (START_SIZE/2), coordYB = (START_SIZE/2);
    private static int ticker;
    
    public static void main(String[] arg0){
        primaryVector = initializeList();
        givePresent(true);
        File file = new File(FILE_LOCATION);
        Scanner scan;
        String parse;
        if(!file.exists())System.out.println("Error in Reading File.");
        
        try {
            scan = new Scanner(file);
            while(scan.hasNextLine()){
                parse = scan.nextLine();
                for(int i = 0; i < parse.length(); i++){
//                    printData();
                    if(parse.charAt(i) == '>'){
                        moveEast();
                    }
                    else if(parse.charAt(i) == '<'){
                        moveWest();
                    }
                    else if(parse.charAt(i) == '^'){
                        moveNorth();
                    }
                    else if(parse.charAt(i) == 'v'){
                       moveSouth();
                    }
                }
            }
            scan.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } 
        

//        printData();
        System.out.println(countHomes()+ "");
    }
    
    private static boolean isRealSanta(){
        return ticker++ % 2 == 0;
    }
    
    private static void moveNorth(){
        boolean isSanta = isRealSanta();
        if(isSanta){
            if(--coordYA < 0)
                decreaseArrayListSizes(false, isSanta);
            givePresent(isSanta);
        }
        else{
            if(--coordYB < 0)
                decreaseArrayListSizes(false, isSanta);
            givePresent(isSanta);
        }
    }
    
    private static void moveSouth(){
        boolean isSanta = isRealSanta();
        if(isSanta){
            if(++coordYA >= currentSize)
                increaseArrayListSizes();
            givePresent(isSanta);
        }
        else{
            if(++coordYB >= currentSize)
                increaseArrayListSizes();
            givePresent(isSanta);
        }
    }
    
    private static void moveEast(){
        boolean isSanta = isRealSanta();
        if(isSanta){
            if(++coordXA >= currentSize)
                increaseArrayListSizes();
            givePresent(isSanta);
        }
        else{
            if(++coordXB >= currentSize)
                increaseArrayListSizes();
            givePresent(isSanta);
        }
    }
    
    private static void moveWest(){
        boolean isSanta = isRealSanta();
        if(isSanta){
            if(--coordXA < 0)
                decreaseArrayListSizes(true, true);
            givePresent(isSanta);
        }
        else{
            if(--coordXB < 0)
                decreaseArrayListSizes(true, false);
            givePresent(isSanta);
        }
    }
    
    private static void givePresent(boolean isSanta){
        if(isSanta){
            Integer temp = (Integer) primaryVector.get(coordYA).get(coordXA);
            primaryVector.get(coordYA).set(coordXA, ++temp);
        }
        else {
            Integer temp = (Integer) primaryVector.get(coordYB).get(coordXB);
            primaryVector.get(coordYB).set(coordXB, ++temp);
        }
    }
    
    private static void decreaseArrayListSizes(boolean isHorizontal, boolean isSanta){
        currentSize++;
        for(int i = 0; i < primaryVector.size(); i++)
            primaryVector.get(i).add(0, 0);
        ArrayList<Integer> temp = new ArrayList(currentSize);
        primaryVector.add(0, temp);
        while(temp.size() < currentSize)
            temp.add(0);
        if(isSanta){
            if(isHorizontal){
                coordXA = 0;
                coordYA++;
                
                coordYB++;
                coordXB++;
            }
            else{
                coordYA = 0;
                coordXA++;
                
                coordYB++;
                coordXB++;
            }
        }
        else{
            if(isHorizontal){
                coordXB = 0;
                coordYB++;
                
                coordYA++;
                coordXA++;
            }
            else{
                coordYB = 0;
                coordXB++;
                
                coordYA++;
                coordXA++;
            }
        }
    }
    
    private static void increaseArrayListSizes(){
        ArrayList<Integer> temp;
        currentSize++;
        while(primaryVector.size() < currentSize){
            temp = new ArrayList(currentSize);
            primaryVector.add(temp);
            while(temp.size() < currentSize)
                temp.add(0);
        }
        for(int i = 0; i < currentSize; i++){
            while(primaryVector.get(i).size() < currentSize)
                primaryVector.get(i).add(0);
        }
    }
    
    private static ArrayList<ArrayList> initializeList(){
        ArrayList<ArrayList> coordVector = new ArrayList(START_SIZE);
        ArrayList<Integer> temp;
        while(coordVector.size() < START_SIZE){
            temp = new ArrayList(START_SIZE);
            coordVector.add(temp);
            while(temp.size() < START_SIZE )
                temp.add(0);
        }
        return coordVector;
    }
    
    private static void printData(){
        System.out.println("\n\n\n\n\n");
        for(int i = 0; i < currentSize; i++){
            for(int j = 0; j < currentSize; j++)
                System.out.print(primaryVector.get(i).get(j) + " ");
            System.out.println("\n");
        }
        System.out.println("\n\n\n\n\n");
    }
    
    private static int countHomes(){
       int count = 0;
       for(int i = 0; i < currentSize; i++){
            for(int j = 0; j < currentSize; j++)
                if(((Integer)primaryVector.get(i).get(j))>0)
                    count++;
        }
       return count;
    }
    
}
